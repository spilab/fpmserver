pm2 start FPMServerComponents\SOWConsultant\TracesReceiver\app.py --name TracesReceiver --interpreter python3
pm2 start FPMServerComponents\SocialMiningAPI\app.py --name SocialMiningAPI --interpreter python3

cd FPMServerComponents\MQTTbroker\mosca
pm2 start broker-mqtt.js --name MQTTbroker
cd ..\..\..

:: cd FPMServerComponents\WebApplication\FPMSocialMiningapplication
:: pm2 start run.bat --name WebApplication
:: cd ..\..\..

FPMServerComponents\WebApplication\FPMSocialMiningapplication\run.bat localhost 4201
