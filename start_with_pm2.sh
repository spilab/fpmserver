pm2 start FPMServerComponents/SOWConsultant/TracesReceiver/app.py --name TracesReceiver --interpreter python3
pm2 start FPMServerComponents/SocialMiningAPI/app.py --name SocialMiningAPI --interpreter python3


cd FPMServerComponents/MQTTbroker/mosca
pm2 start broker-mqtt.js --name MQTTbroker
cd ../../..

#cd FPMServerComponents/WebApplication/FPMSocialMiningapplication
#pm2 start run.sh --name WebApplication --interpreter bash
#cd ../../..

cd FPMServerComponents/WebApplication/FPMSocialMiningapplication
./run.sh localhost 4201
