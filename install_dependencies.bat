npm install 
:: for install pm2 process manager

::python dependencies
python3 -m pip install -r requirements.txt

::MQTT nodejs broker dependencies
cd FPMServerComponents\MQTTbroker\mosca
npm install
cd ..\..\..


::Angular web application dependencies
cd FPMServerComponents\WebApplication\FPMSocialMiningapplication
npm install
cd ..\..\..