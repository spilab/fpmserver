import os
from pm4py.objects.log.importer.xes import factory as xes_importer
import json

from pm4py.visualization.heuristics_net import factory as hn_vis_factory
from pm4py.algo.discovery.heuristics import factory as heuristics_miner
from pm4py.evaluation import factory as evaluation_factory

from FPMServerComponents.xes_utils.xes_utils import combine_logs
import time
import numpy as np
import pickle
import base64

os.environ["PATH"] += os.pathsep + 'C:/Program Files (x86)/Graphviz2.38/bin/'


import sys
from pathlib import Path
file = Path(__file__). resolve()
package_root_directory = file.parents [2]
sys.path.append(str(package_root_directory))

from FPMServerComponents.PARAMS import QUERIES_LOGS_DIRECTORY,MODELS_DIRECTORY, INTEGRATED_LOG_NAME
DATA = QUERIES_LOGS_DIRECTORY
OUTPUT_FOLDER= MODELS_DIRECTORY
TOTAL_LOG_LOGNAME = INTEGRATED_LOG_NAME

def read_query_folder(query):
    query_path = os.path.join(DATA, query)
    logs = os.listdir(query_path)
    print(logs)
    if not os.path.exists(os.path.join(OUTPUT_FOLDER, query)):
        os.makedirs(os.path.join(OUTPUT_FOLDER, query))
    if str(TOTAL_LOG_LOGNAME) in logs:
        logs.remove(str(TOTAL_LOG_LOGNAME))
    return query_path, logs

def create_query_folder(query):
    if not os.path.exists(os.path.join(DATA, query)):
        os.makedirs(os.path.join(DATA, query))

def generate_log_total(path, logs):
    time_init = time.time()
    logs_paths = []
    for log in logs :
        logs_paths.append(os.path.join(path,log))
    combine_logs(logs_paths, path, str(TOTAL_LOG_LOGNAME))
    print("Generating total log with ",str(len(logs))," logs")
    return time.time() - time_init


def generate_model(query, logname):
    time_log_init = time.time()
    log = xes_importer.import_log(os.path.join(DATA, query, logname))
    print(str(logname) + " - Number of traces:", len(log))

    heu_net = heuristics_miner.apply_heu(log, parameters={"dependency_thresh": 0.5})
    parameters = {"format": "png"}
    gviz = hn_vis_factory.apply(heu_net, parameters=parameters)
    hn_vis_factory.save(gviz, os.path.join(str(OUTPUT_FOLDER), str(query), str(logname).replace("xes", "png")))
    time_model = time.time() - time_log_init
    #TODO mandar modelos a pickle
    # name_pickle = os.path.join(str(OUTPUT_FOLDER), str(query), str(logname).replace("xes", "pickle"))
    # pickle_file = file(str(name_pickle), 'w')
    # pickle.dump(heu_net, pickle_file)

    net, im, fm = heuristics_miner.apply(log, parameters={"dependency_thresh": 0.5})
    evaluation_model = evaluate(log, net, im, fm)
    number_arcs =len(heu_net.dfg.items())
    number_act = len(heu_net.activities)
    sum_arcs = get_occurences(heu_net.dfg)

    return time_model, evaluation_model, number_arcs, number_act, sum_arcs


def generate_individual_models(query, logs):
    time_init = time.time()
    times_peer_log = []
    evaluations_logs = []
    number_arcs = []
    number_act = []
    sum_arcs = []
    for logname in logs:
        time_model, evaluation_model, n_arcs, n_act, s_arcs = generate_model(query, logname)

        times_peer_log.append(time_model)
        evaluations_logs.append(evaluation_model)
        number_arcs.append(n_arcs)
        number_act.append(n_act)
        sum_arcs.append(s_arcs)
    sum_times = time.time() - time_init

    return number_act, number_arcs, sum_arcs, evaluations_logs, sum_times, times_peer_log


def generate_total_log_model(query):
    time_model, evaluation_model, n_arcs, n_act, s_arcs = generate_model(query, TOTAL_LOG_LOGNAME)
    return n_act, n_arcs, s_arcs, evaluation_model, time_model


def evaluate(log, net, ini_mark, fin_mark):
    evaluation_result = evaluation_factory.apply(log, net, ini_mark, fin_mark)
    return evaluation_result


def get_occurences(arcs):
    total = 0
    for arc in arcs:
        total = total + arcs[arc]
    return total


def show_statistics(times):
    for query in times:
        print(query)
        for metric in times[query]:
            print(metric, ':', times[query][metric])

def print_dict(f, metric, data):
    for metric2 in data:
        if isinstance(data[metric2],dict):
            print_dict(f, str(metric)+"."+ str(metric2), data[metric2])
        else:
            f.write(str(metric)+"."+ str(metric2) + ':' + str(data[metric2]) + "\n")

def print_statistics(times, path):
    try:
        with open(os.path.join(path,'statistics.txt'), 'w') as f:
            for query in times:
                f.write("--"+str(query)+"\n")
                for metric in times[query]:
                    if isinstance(times[query][metric],dict):
                        print_dict(f,metric, times[query][metric])
                    else:
                        f.write(str(metric)+':'+str(times[query][metric])+"\n")
            f.close()
    except:
        print("File",os.path.join(path,'statistics.txt'),"cannot be opened for write")


def append_dicts_recursive(dict1, dict2):
    dict_result={}
    for key in dict1:
        if key in dict2:
            dict_result[key] = append_dicts_recursive(dict1[key],dict2[key])
        else:
            dict_result[key] = dict1[key]
    for key in dict2:
        if key not in dict1:
            dict_result[key] = dict2[key]
    return dict_result
    # return {**dict1, **dict2}

def read_statistics(query):
    path = os.path.join(str(OUTPUT_FOLDER), str(query))
    times = {}
    try:
        with open(os.path.join(path,'statistics.txt'), 'r') as f:
            lines = f.readlines()
            f.close()
            query=""
            for line in lines:
                line = line.replace("\n","")
                if line[0:2]=="--":
                    query = line[2:]
                    times[query] = {}
                else:
                    parts = line.split(":")
                    parts_aux = parts[0].split(".")
                    value = parts[1]
                    for prefix in reversed(parts_aux):
                        value = {prefix:value}
                    if prefix in times[query]:
                        times[query][prefix] = append_dicts_recursive(times[query][prefix], value[prefix])
                    else:
                        times[query][prefix] = value[prefix]
    except FileNotFoundError:
        print("File",os.path.join(path,'statistics.txt'),"cannot be opened for read")
    except Exception as e:
        print("File",os.path.join(path,'statistics.txt'),"present any problem that impedes its reading")
        print(e)
    return times


def get_general_model_in_b64(query):
    try:
        with open(os.path.join(str(OUTPUT_FOLDER), str(query), str(TOTAL_LOG_LOGNAME).replace("xes", "png")), "rb") as image_file:
            encoded_bytes = base64.b64encode(image_file.read())
            encoded_string = encoded_bytes.decode("utf-8")
            return encoded_string
    except FileNotFoundError:
        return None

def get_user_model_in_b64(query, user_id):
    name_file = str(user_id)+str(".png")
    with open(os.path.join(str(OUTPUT_FOLDER), str(query), str(name_file)), "rb") as image_file:
        encoded_bytes = base64.b64encode(image_file.read())
        encoded_string = encoded_bytes.decode("utf-8")
        return encoded_string
    return None


def read_users_meet_pattern(query):
    query_path, logs = read_query_folder(query)

    if TOTAL_LOG_LOGNAME in logs:
        logs.remove(TOTAL_LOG_LOGNAME)
    if "statistics.txt" in logs:
        logs.remove("statistics.txt")

    users = []

    for log in logs:
        users.append(log.replace(".xes",""))
    return users


def read_users_of_model(query):
    output_query_path = os.path.join(OUTPUT_FOLDER, query)
    users = []

    if os.path.exists(output_query_path):
        logs = os.listdir(output_query_path)
        total_model = TOTAL_LOG_LOGNAME.replace(".xes",".png")
        if str(total_model) in logs:
            logs.remove(str(total_model))
        if "statistics.txt" in logs:
            logs.remove("statistics.txt")
        for log in logs:
            users.append(log.replace(".png",""))
    return users



def read_queries():
    data_contents = os.listdir(DATA)
    return data_contents


def generate_all_models():
    try:
        if not os.path.exists(OUTPUT_FOLDER):
            os.makedirs(OUTPUT_FOLDER)
        data_contents = os.listdir(DATA)
        print(data_contents)
        total_times = {}
        for query in data_contents:
            if os.path.isdir(os.path.join(DATA,query)):
                query_path, logs = read_query_folder(query)
                print(query_path)
                query_stats = {}
                time_generate_log_total = generate_log_total(query_path, logs)
                _, _, _, _, time_individual_models, times_logs = generate_individual_models(query,logs)
                number_act, number_arcs, sum_arcs, evaluations_logs, time_total_log_model = generate_total_log_model(query)

                query_stats["activities"] = number_act
                query_stats["arcs"] = number_arcs
                query_stats["sum_arcs_weights"] = sum_arcs
                query_stats["evaluation"] = evaluations_logs
                query_stats["generate_log_total"] = time_generate_log_total
                query_stats["time_total_log_model"] = time_total_log_model
                query_stats["sum_time_individual_models"] = time_individual_models
                query_stats["avg_time_individual_models"] = np.average(np.array(times_logs))

                total_times[query] = query_stats

        show_statistics(total_times)
        for query in data_contents:
            times = {str(query):total_times[query]}
            print_path= os.path.join(str(OUTPUT_FOLDER), str(query))
            print_statistics(times, print_path)
            print(read_statistics(query))
    except:
        raise Exception

def generate_models_of_query(query):

    try:
        if not os.path.exists(OUTPUT_FOLDER):
            os.makedirs(OUTPUT_FOLDER)
        total_times = {}
        if os.path.isdir(os.path.join(DATA,query)):
            query_path, logs = read_query_folder(query)
            query_stats = {}
            time_generate_log_total = generate_log_total(query_path, logs)
            _, _, _, _, time_individual_models, times_logs = generate_individual_models(query,logs)
            number_act, number_arcs, sum_arcs, evaluations_logs, time_total_log_model = generate_total_log_model(query)

            query_stats["activities"] = number_act
            query_stats["arcs"] = number_arcs
            query_stats["sum_arcs_weights"] = sum_arcs
            query_stats["evaluation"] = evaluations_logs
            query_stats["generate_log_total"] = time_generate_log_total
            query_stats["time_total_log_model"] = time_total_log_model
            query_stats["sum_time_individual_models"] = time_individual_models
            query_stats["avg_time_individual_models"] = np.average(np.array(times_logs))

            total_times[query] = query_stats

            print_statistics(total_times, os.path.join(str(OUTPUT_FOLDER), str(query)))
            show_statistics(total_times)
        else:
            raise FileNotFoundError
    except FileNotFoundError:
        raise FileNotFoundError
    except Exception:
        raise Exception


# def generate_124_log_model():
#     time_model, evaluation_model, n_arcs, n_act, s_arcs = generate_model("_AllTracesCombinations", "log_124.xes")
#     return n_act, n_arcs, s_arcs, evaluation_model, time_model
#
# def generate_test_log_model():
#     time_model, evaluation_model, n_arcs, n_act, s_arcs = generate_model("Test", "log_total.xes")
#     return n_act, n_arcs, s_arcs, evaluation_model, time_model
#
#
#
# if __name__ == "__main__":
#     print("Init")
#     number_act, number_arcs, sum_arcs, evaluations_logs, time_total_log_model = generate_124_log_model()
#     print(number_act, number_arcs, sum_arcs, evaluations_logs, time_total_log_model)
#     number_act, number_arcs, sum_arcs, evaluations_logs, time_total_log_model = generate_test_log_model()
#     print(number_act, number_arcs, sum_arcs, evaluations_logs, time_total_log_model)
#     print("End")


if __name__ == "__main__":
    print("Init")
    generate_all_models()
    print("End")