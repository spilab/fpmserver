import os
from pathlib import Path
file = Path(__file__). resolve()
root = file.parents [0]

QUERIES_LOGS_DIRECTORY = os.path.join(root,"data/querieslogs")
MODELS_DIRECTORY = os.path.join(root,"data/queriesmodels")
INTEGRATED_LOG_NAME = "log_total.xes"