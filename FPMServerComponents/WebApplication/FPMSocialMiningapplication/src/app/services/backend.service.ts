import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse, HttpHeaders } from '@angular/common/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import { Query } from '../models/query';
import { GLOBAL } from './global';

@Injectable()
export class BackendService {
  private headers: HttpHeaders = new HttpHeaders({'Content-Type': 'application/json'});
  public url:string;
  private auth:string;

  constructor( private _http:HttpClient){
    this.url= GLOBAL.base_url;
  }



  postQuery(query:Query):Observable<any>{
    let headers= new HttpHeaders({'Content-Type':'application/json'});
    return this._http.post(this.url+'/query?p='+query.name, {headers: headers, observe : 'response'});//.map(res => res.json());
  }

  getQueries():Observable<any>{
    let headers= new HttpHeaders({'Content-Type':'application/json'});
    return this._http.get(this.url+'/query',{headers: headers, observe : 'response'});
  }


  getUsersByQuery(query:Query):Observable<any>{
    let headers= new HttpHeaders({'Content-Type':'application/json'});
    return this._http.get(this.url+'/query/users?p='+query.name,{headers: headers, observe : 'response'});
  }

  generateAllModels():Observable<any>{
    let headers= new HttpHeaders({'Content-Type':'application/json'});
    return this._http.get(this.url+'/model/generate?p=*',{headers: headers, observe : 'response'});
  }


  generateModelsOfQuery(query:Query):Observable<any>{
    let headers= new HttpHeaders({'Content-Type':'application/json'});
    return this._http.get(this.url+'/model/generate?p='+query.name,{headers: headers, observe : 'response'});
  }


  getModelOfQuery(query:Query):Observable<any>{
    let headers= new HttpHeaders({'Content-Type':'application/json'});
    return this._http.get(this.url+'/model/retrieve?p='+query.name,{headers: headers, observe : 'response'});
  }

  getUserModelOfQuery(user_id:string, query:Query):Observable<any>{
    let headers= new HttpHeaders({'Content-Type':'application/json'});
    return this._http.get(this.url+'/model/retrieve/'+user_id+'?p='+query.name,{headers: headers, observe : 'response'});
  }
}
