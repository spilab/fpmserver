import {Statistics} from './statistics';

export class Model{
  constructor(
    public image: string,
    public statistics: Statistics,
    public users_id: Array<string>
  ){};
}
