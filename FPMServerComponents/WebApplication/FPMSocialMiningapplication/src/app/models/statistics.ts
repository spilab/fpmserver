export class Statistics{
    constructor(
      public activities: string,
      public arcs: string,
      public avg_time_individual_models: string,
      public evaluation: Evaluation,
      public generate_log_total: string,
      public sum_arcs_weights: string,
      public sum_time_individual_models: string,
      public time_total_log_model: string
    ){};
  }


class Evaluation{
  constructor(
    public fitness: Fitness,
    public fscore: string,
    public generalization: string,
    public metricsAverageWeight: string,
    public precision: string,
    public simplicity: string
  ){};
}

class Fitness{
  constructor(
    public average_trace_fitness : string,
    public log_fitness : string,
    public perc_fit_traces : string
  ){};
}