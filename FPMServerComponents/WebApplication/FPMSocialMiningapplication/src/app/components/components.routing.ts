import { Routes } from '@angular/router';

import { QueriesSenderComponent } from './queriessender/queriessender.component';


export const ComponentsRoutes: Routes = [
  {
    path: 'queriessender',
    component: QueriesSenderComponent
  }
];
