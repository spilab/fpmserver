import { Component, OnInit } from '@angular/core';
import { Query } from '../../models/query';
import { BackendService } from '../../services/backend.service';

@Component({
  selector: 'queries-sender',
  templateUrl: './queriessender.component.html',
  styleUrls: ['./queriessender.component.scss'],
  providers: [BackendService]

})
export class QueriesSenderComponent implements OnInit{

  public queries: Array<Query>;
  public error_save:string;
  public newQuery:Query;

	constructor(private _backend:BackendService){
    this.queries = [];
    this.error_save=null;
    this.newQuery = new Query("");
  }
  
  ngOnInit(){
    this.loadQueries();
  }


	loadQueries():void{
    this.queries = [];
		this._backend.getQueries().subscribe(
			response => {
				if(response.status==200 || response.status==204){
					if(response.status==204){
					console.log("There are not previous queries");
					}
					else{
            response.body["queries"].forEach(element => {
              this.queries.push(new Query(element));
            });
						console.log(this.queries);
					}
				}
		}, error =>{
			console.log(error);
			alert("Something's not right with the Social Mining API. Sorry");
		});
  }
  

  sendQuery():void{
    var query = new Query(this.newQuery.name.replace(/\s\s+/g, ' '));
    
    let found=false;
    for(let i=0; i<this.queries.length && !found; i++){
      let element = this.queries[i];
      if (element.name.replace(" ", "")==query.name.replace(" ","")){
        this.queryYetExists(query);
        found=true;
      }  
    };

    if(!found){
      this._backend.postQuery(query).subscribe(response => {
        this.loadQueries();
      }, error =>{
        console.log(error);
        alert("Something's not right with the Social Minin API. Sorry");
      });
    }
  }

  queryYetExists(query:Query){
    var r = confirm("This query yet exists! Do you want to resend it?");
    if (r == true) {
      this.resendQueryAux(query);
      alert("Query resend");
    }
  }
  resendQuery(query:Query){
    var r = confirm("Are you sure that want to resend it?");
    if (r == true) {
      this.resendQueryAux(query);
      alert("Query resend");
    }
  }

  //TODO delete previous data??
  resendQueryAux(query:Query){
    this._backend.postQuery(query).subscribe(response => {
      this.loadQueries();
    }, error =>{
      console.log(error);
      alert("Something's not right with the Social Minin API. Sorry");
    });
  }

}
