import { Component, AfterViewInit, OnInit } from '@angular/core';

import * as Chartist from 'chartist';
import { ChartType, ChartEvent } from 'ng-chartist';
import { Query } from '../models/query';
import { Model } from '../models/model';
import { BackendService } from '../services/backend.service';
declare var require: any;

const data: any = require('./data.json');

export interface Chart {
	type: ChartType;
	data: Chartist.IChartistData;
	options?: any;
	responsiveOptions?: any;
	events?: ChartEvent;
}

@Component({
	selector: 'app-dashboard',
	templateUrl: './dashboard.component.html',
	styleUrls: ['./dashboard.component.scss'],
	providers: [BackendService]
})
export class DashboardComponent implements OnInit, AfterViewInit {
	

	public queries:Array<Query>=[];
	public selectedUser:string=null;
	public selectedQuery:Query=null;
	public modelExists:boolean=true;
	public queryModel:Model=null;
	public userModel:string=null;
	public users:Array<string>=null;

	constructor(private _backend:BackendService){
	}


	ngOnInit(){
		console.log("Loading existing queries");
		this.loadQueries();
	}


	ngAfterViewInit() {	
	}


	onChangeSelectedUser(){
		if (this.selectedUser!=null){
			//TODO retrieve model of the user and show
			this.loadUserModel(this.selectedUser, this.selectedQuery);
		}/*else{
			//show general model
		}*/
	}

	loadUserModel(user:string, query:Query){
		this.userModel=null;
		this._backend.getUserModelOfQuery(user,query).subscribe(
			response => {
				if(response.status==200 || response.status==204){
					if(response.status==204){
						this.userModel=null;
					}
					else{		
						this.userModel=response.body["image"];
					}
				}
		}, error =>{
			if(error.status==400){
				this.userModel=null;
			}
			else{
				console.log(error);
				this.userModel=null;
				alert("Something's not right with the Social Mining API. Sorry");
			}
		});
	}

	selectGeneral(){
		this.selectedUser=null;
		this.userModel=null;
		this.onChangeSelectedUser();
	}


	selectQuery(query:Query){
		this.selectedQuery=query;
		this.loadModel(query);
		this.loadUsers(query);
		this.selectGeneral();
	}
	

	loadQueries():void{
		this.queries = [];
			this._backend.getQueries().subscribe(
				response => {
					if(response.status==200 || response.status==204){
						if(response.status==204){
						console.log("There are not previous queries");
						}
						else{
				response.body["queries"].forEach(element => {
				  this.queries.push(new Query(element));
				});
							console.log(this.queries);
						}
					}
			}, error =>{
				console.log(error);
				alert("Something's not right with the Social Mining API. Sorry");
			});
	  }

	loadUsers(query:Query):void{
		this.users=null;
		this._backend.getUsersByQuery(query).subscribe(
			response => {
				if(response.status==200 || response.status==204){
					if(response.status==204){
					console.log("There are not users that have sent their data");
					this.users=[];
					}
					else{
						this.users=response.body["users_id"];
					}
				}
		}, error =>{
			console.log(error);
			this.users=[];
			alert("Something's not right with the Social Mining API. Sorry");
		});
	}


	loadModel(query:Query):void{
		this.modelExists=true;
		this.queryModel=null;
		this._backend.getModelOfQuery(query).subscribe(
			response => {
				if(response.status==200 || response.status==204){
					if(response.status==204){
						this.modelExists=false;
						this.queryModel=null;
					}
					else{
						this.modelExists=true;
						this.queryModel=response.body;
					}
				}
		}, error =>{
			if(error.status==400){
				this.modelExists=false;
				this.queryModel=null;
			}
			else{
				console.log(error);
				this.modelExists=true;
				this.queryModel=null;
				alert("Something's not right with the Social Mining API. Sorry");
			}
		});
	}

	generateModel(){
		var query = this.selectedQuery;
		this._backend.generateModelsOfQuery(query).subscribe(
			response => {
				if(response.status==200 || response.status==201){
					this.loadModel(query);
				} else{
					alert("Something's not right generating model. Sorry");
				}
		}, error =>{
			console.log(error);
			alert("Something's not right with the Social Mining API. Sorry");
		});
	}


	generateAllModels(){
		var r = confirm("Do you want to generate/update models of all queries really?");

		if (r==true){
			this._backend.generateAllModels().subscribe(
				response => {
					if(response.status==200 || response.status==201){
						if (this.selectedQuery!=null){
							this.loadModel(this.selectedQuery);
							this.selectGeneral();
						}
					} else{
						alert("Something's not right generating models. Sorry");
					}
			}, error =>{
				console.log(error);
				alert("Something's not right with the Social Mining API. Sorry");
			});
		}
	}

	swipe(src, name) {
		var image = new Image();
        image.src = src;

        var w = window.open("",name);
        w.document.write(image.outerHTML);
		w.stop();
	 }
}
