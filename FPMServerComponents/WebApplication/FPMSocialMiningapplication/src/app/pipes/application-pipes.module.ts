// application-pipes.module.ts
// other imports
import { NgModule } from '@angular/core';
import { SafeHtml } from './SafeHtml';

@NgModule({
  imports: [
    // dep modules
  ],
  declarations: [ 
    SafeHtml
  ],
  exports: [
    SafeHtml
  ]
})
export class ApplicationPipesModule {}