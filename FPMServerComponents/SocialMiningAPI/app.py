from flask import Flask, request, jsonify
from flask_cors import CORS

import os

import sys
from pathlib import Path
file = Path(__file__). resolve()
package_root_directory = file.parents [2]
sys.path.append(str(package_root_directory))

from FPMServerComponents.PARAMS import QUERIES_LOGS_DIRECTORY
from FPMServerComponents.GLOBALS import IP_SERVER,SOCIAL_MINING_API_PORT
from FPMServerComponents.SOWConsultant.QueriesSender import QueriesSenderClient
from FPMServerComponents.SocialPMModule.generate_modelsv2 import generate_all_models,generate_models_of_query, read_statistics, get_general_model_in_b64, read_users_meet_pattern, get_user_model_in_b64, create_query_folder, read_queries, read_users_of_model

BASE_DIR = QUERIES_LOGS_DIRECTORY


def create_dir(dirs):
    path = BASE_DIR
    for dir in dirs:
        path = os.path.join(path,dir)
    if not os.path.exists(path):
        os.makedirs(path)
    return path

app = Flask(__name__)
CORS(app)


@app.route('/')
def hello_world():
    return 'Hello World!'


@app.route('/query', methods=["POST"])
def send_query():
    pattern = str(request.args.get('p'))

    create_query_folder(pattern)

    response = QueriesSenderClient.sendQuery(pattern)

    if response == True:
        return "", 201
    else:
        return "", 500

@app.route('/query', methods=["GET"])
def get_queries():

    queries = read_queries()
    if queries!=None:
        data = {"queries": queries}
        if len(queries)==0:
            return jsonify(data), 204
        return jsonify(data), 200
    else:
        return "", 500


@app.route('/query/users', methods=["GET"])
def get_users_by_query():
    pattern = str(request.args.get('p'))

    users_id = read_users_meet_pattern(pattern)
    if users_id!=None:
        data = {"users_id": users_id}
        return jsonify(data), 200
    else:
        return "", 500



@app.route('/model/generate', methods=["get"])
def generate_model():
    pattern = str(request.args.get('p'))


    try:
        #all
        if pattern == "*":
            generate_all_models()
        else:
            generate_models_of_query(pattern)
    except FileNotFoundError:
        return "",400
    except Exception:
        return "",500

    return "",200

@app.route('/model/retrieve', methods=["get"])
def get_model():
    pattern = str(request.args.get('p'))

    try:
        image = get_general_model_in_b64(pattern)
        if image == None:
            return "",400
        users_id = read_users_of_model(pattern)
        statistics = read_statistics(pattern)

        data = {"image": image,
                "statistics": statistics[pattern],
                "users_id": users_id}
        # print(data)
        return jsonify(data), 200

    except Exception:
        return "", 500


@app.route('/model/retrieve/<userid>', methods=["get"])
def get_model_of_user(userid):
    pattern = str(request.args.get('p'))

    try:
        image = get_user_model_in_b64(pattern, userid)
        if image == None:
            return "", 400

        data = {"image": image}
        return jsonify(data), 200

    except Exception:
        return "", 500



if __name__ == '__main__':
    app.run(host=IP_SERVER, port=SOCIAL_MINING_API_PORT)
