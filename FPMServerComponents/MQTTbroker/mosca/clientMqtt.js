var mqtt = require('mqtt')


// Movil 192.168.0.131:1883
// Movil 192.168.43.1:1883
// Local localhost:1883
var client  = mqtt.connect('mqtt://192.168.1.94:1883');

client.on('connect', function () {

    console.log("Connected")
})


  client.on('message', function (topic, message) {
    console.log("Result received from MQTT")
    message= message.toString('utf8')

   console.log(message)

});

//client.publish('EventAlerts', JSON.stringify(paramsMqtt));


//client.subscribe('result');
//client.subscribe('mytopic/test');
// Suscribirse al canal de respuestas
//client.subscribe('mytopic/testResponse');
//client.subscribe('HealthAlertsAndroid');


console.log("---- Tests");

// Baterías de tests. Comentar/Descomentar cada bloque de código.
// Los dos últimos tests son siempre para comprobar el control de errores.

// Health Alerts Tests  -- OJO el nombre es Event Alerts

var paramsMqtt1={"resource":"Query",
            "method": "requestGet",
            "sender": "0",
            "params": {
                "pattern":"Housework complete - Relaxing complete"
            }
};
client.publish('FPMPatternQueryManager', JSON.stringify(paramsMqtt1));
