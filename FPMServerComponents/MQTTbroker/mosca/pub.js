var mqtt = require('mqtt')


// Movil 192.168.0.131:1883
// Movil 192.168.43.1:1883
// Local localhost:1883
var client  = mqtt.connect('mqtt://63.35.75.97:1883', {username:'monactapp',password:'spilab2020'});
 
client.on('connect', function () {
    
    console.log("Connected")
})

 
  client.on('message', function (topic, message) {
    console.log("Result received from MQTT")
    message= message.toString('utf8')
    
   console.log(message)

});

console.log("---- Tests");

// Baterías de tests. Comentar/Descomentar cada bloque de código.

var paramsMqtt1={
  "resource": "Event",
  "method": "postEvent",
  "sender": "ClienteVSCODE",
  "params": {
    "event":{
      "id": 1,
      "title": "Need help!",
      "description": "I have fallen.",
      "location": {
        "latitude": 39.47903,
        "longitude": -6.342139,
        "radius": 20
      }
    }
  }
};
client.publish('ClienteVSCODE', JSON.stringify(paramsMqtt1));
var paramsMqtt2={
  "resource": "Location",
  "method": "getUser",
  "sender": "ClienteVSCODE",
  "params": {}
};
var paramsMqtt3={
  "resource": "Location",
  "method": "getTemperature",
  "sender": "ClienteVSCODE",
  "params": {}
};

client.publish('ClienteVSCODE', JSON.stringify(paramsMqtt2));
setTimeout(() => { client.publish('ClienteVSCODE', JSON.stringify(paramsMqtt3)); }, 5000);
