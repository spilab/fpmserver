import paho.mqtt.client as paho
import json
import struct

BROKER="localhost"
PORT=1883
TOPIC = "FPMPatternQueryManager"

def on_publish(client,userdata,result):             #create function for callback
    print("data published \n")
    pass

def sendQuery(query):

    data = {"resource":"Query",
            "method": "requestGet",
            "sender": "0",
            "params": {
                "pattern":str(query)
            }
            }

    payload = json.dumps(data)

    client1= paho.Client("Server")                           #create client object
    client1.on_publish = on_publish                          #assign function to callback
    client1.connect(BROKER,PORT)                                 #establish connection
    ret= client1.publish(str(TOPIC),struct.pack(payload))#publish
    return ret.is_published()


if __name__=="__main__":
    sendQuery("Housework complete - Relaxing complete")