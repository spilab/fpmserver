import paho.mqtt.client as paho
from Naked.toolshed.shell import execute_js, muterun_js
import json
import struct
import sys
import os
from pathlib import Path

file = Path(__file__). resolve()
package_root_directory = file.parents [3]
sys.path.append(str(package_root_directory))

from FPMServerComponents.GLOBALS import BROKER_IP

BROKER=str(BROKER_IP)
PORT=1883
HOST = "mqtt://"+str(BROKER_IP)+":1883"
TOPIC = "FPMPatternQueryManager"


def sendQuery(query):

    data = {"resource":"Query",
            "method": "requestGet",
            "sender": "0",
            "params": {
                "pattern":str(query)
            }
            }

    pathname = os.path.dirname(sys.argv[0])
    pathname = os.path.abspath(pathname)
    # print('path =', pathname)
    # print('full path =', os.path.abspath(pathname))
    if pathname.find('FPMServer') >=0:
        print("pathname_ini=",pathname)
        pathname = pathname[:pathname.find('FPMServer')]
        print("pathname=",pathname)
        file_script = os.path.join(str(pathname), "FPMServer\FPMServerComponents\SOWConsultant\QueriesSender\js\clientMqtt.js")
        print("files_script=",file_script)
    else:
        file_script = 'js/clientMqtt.js'

    success = execute_js(file_script, arguments=str(HOST)+" "+str(query))

    print(success)
    return success


if __name__=="__main__":
    sendQuery("Housework complete - Relaxing complete")