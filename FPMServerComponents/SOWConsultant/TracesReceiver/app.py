from flask import Flask, request
import os

import sys
from pathlib import Path
file = Path(__file__). resolve()
package_root_directory = file.parents [3]
sys.path.append(str(package_root_directory))

from FPMServerComponents.xes_utils.xes_utils import save_traces
from FPMServerComponents.PARAMS import QUERIES_LOGS_DIRECTORY
from FPMServerComponents.GLOBALS import IP_SERVER,SOW_CONSULTANT_TRACES_RECEIVER_PORT

BASE_DIR = QUERIES_LOGS_DIRECTORY


def create_dir(dirs):
    path = BASE_DIR
    for dir in dirs:
        path = os.path.join(path,dir)
    if not os.path.exists(path):
        os.makedirs(path)
    return path

app = Flask(__name__)

@app.route('/')
def hello_world():
    return 'Hello World!'


@app.route('/traces', methods=["POST"])
def get_traces():
    body = request.get_json()

    username = body["username"]
    pattern = body["pattern"]
    dir = create_dir([pattern])
    traces = body["traces"]

    saved = save_traces(dir, username, traces)
    if saved:
        return "", 201
    else:
        return "", 500


if __name__ == '__main__':
    app.run(host=IP_SERVER, port=SOW_CONSULTANT_TRACES_RECEIVER_PORT)
