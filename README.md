# Social Mining Components #

This component is the one that is deployed on the server. It is actually a set of components ---implemented in Python or JavaScript--- that work together. Each of them is in charge of part of the functions to be performed by the Social Process Mining component:

- **Web Application** offers a frontend for administrators to request data from smartphones, visualize the users that meet a pattern, generate models with the traces of each one of them separately or of the whole group, and visualize them together with statistics about their size and quality. All this in a user-friendly way, through a graphical interface.
    
- **Social Mining API** is a REST API that works as an access point to the rest of the components that make up the backend of the Social Process Mining component. It provides all the operations that are consumed by the Web Application. It also allows Federated Process Mining to be offered as a service to third-parties or users' smartphones to be the ones interested in accessing the data of a given Social Workflow.

- **SOW consultant** is responsible for sending trace requests to smartphones, and for retrieving and storing the traces sent in response.
    
- **Social Process Mining Module** is in charge of performing the process mining tasks on the data coming to the server.

## Before run the components ##

Before launching the components it is important to read these instructions carefully and follow the steps indicated here to deploy each of the components that make up the Federated Process Mining server.


### 0. Pre-requirements ###

It is necessary to have a computer where it is installed:
- NodeJS
- npm
- Python 3
- pip 3

### 1. Update server parameters ###

Prior to any change, the Globals.py parameters must be modified. In particular, the IP address of the server. It is recommended to leave the default component ports.

This IP must also be changed in the *base_url* variable of the "src/app/services/global.ts" file in the web application. The web application will try to connect to the Social Mining API through port 8082 by default. If this value has been changed in Globals.py, it must be changed in globals.ts as well. 

The server IP, the MQTT server port (1883 by default) and the Traces Receiver API port will be required for the Individual Mining Component.

The web application is always deployed on localhost, on port 4200. To modify these values (for example because you want to access the application from outside the server, modify in the script "FPMServerComponents/WebApplication/FPM-SocialMining-application/run.sh" the instruction "ng serve --host=localhost --port=4200" por "ng serve --host=<ip_host> --port=<new_port>".

### 2. Install dependencies ###

To install the dependencies required by the different components, run the following script
 *install_dependencies.bat* (Windows) or *install_dependencies.sh* (Linux/MacOS).

 You also need to have Graphviz installed to generate PM4PY models (see PM4PY installation page).

### 3. Run components ###

To manage the execution of some of the processes, the PM2 package manager will be used, installed together with the rest of the requirements.

#### *Option 1: Use start_with_pm2 scripts (Alpha version, not recommended)* ####

There is an option to start and stop all processes automatically with a single script. However, some components give problems when deployed in this way. We are working on solving these problems.

- To launch the processes: *start_with_pm2.bat* (Windows) or *start_with_pm2.sh* (Linux/MacOS)
- To stop processes previously started: *stop_with_pm2.bat* (Windows) or *stop_with_pm2.sh* (Linux/MacOS)

#### *Option 2: Manual deployment (recommended)* ####

Run the following commands:

- Run TracesReceiver API of SOW Consultant
    - pm2 start FPMServerComponents/SOWConsultant/TracesReceiver/app.py --name TracesReceiver --interpreter python3
- Run MQTT broker
    - cd FPMServerComponents/MQTTbroker/mosca
    - pm2 start broker-mqtt.js --name MQTTbroker
    - cd ../../..
- Run Social Mining API
    - python3 FPMServerComponents/SocialMiningAPI/app.py (add & at the end to run in background)
- Run Web Application
    - cd FPMServerComponents/WebApplication/FPMSocialMiningapplication
    - ./run.sh localhost 4201
